use("rubberMat")

const HALL_CODE = db.halls.aggregate([{$sample: {size: 1}}]).toArray()[0].code

const DTO_IN = {
    hallCode: HALL_CODE
}

console.log("DtoOut:", closeHall(DTO_IN))

async function closeHall(dtoIn) {
    const hall = await _tryFindHall(dtoIn.hallCode)
    await _tryUpdatePresses(hall._id)
    return await _tryDeleteHall(hall)
}

async function _tryFindHall(code) {
    let hall
    try {
        // Because code is not unique
        hall = await db.halls.aggregate({$match: {code: code}}, {$sample: {size: 1}}).toArray()[0]
    } catch (e) {
        console.log(e)
    }
    return hall
}

async function _tryUpdatePresses(hallId) {
    try {
        const result = await db.presses.updateMany(
            {hallId: hallId},
            {$set: {hallId: null}}
        )
        console.log("Update presses result:", result)
    } catch (e) {
        console.log(e)
    }
}

async function _tryDeleteHall(hall) {
    let result
    try {
        result = await db.halls.deleteOne(hall)
    } catch (e) {
        console.log(e)
    }
    return result
}