use("rubberMat")

const FORM_INPUT = {
    code: 'FM-EE29-821-661520',
    mouldingType: 'Injection',
    weight: 3089.06,
    cycleLength: 160,
    numberAvailable: 5
}
const PRODUCT_CODE = db.products.aggregate([{$sample: {size: 1}}]).toArray()[0].code
const PRESSES = db.presses.aggregate([{$match: {mouldingType: "Injection"}}, {$sample: {size: 5}}]).toArray()
const DTO_IN = {
    form: FORM_INPUT,
    productCode: PRODUCT_CODE,
    pressCodeList: _getPressCodeList(PRESSES)
}

console.log("DtoOut:", addForm(DTO_IN))

async function addForm(dtoIn) {
    const product = await _tryFindProductByCode(dtoIn.productCode)
    const form = _fillForm(dtoIn.form, product._id)
    const saveFormResult = await _trySaveForm(form)
    await _tryUpdatePresses(dtoIn.pressCodeList, saveFormResult.insertedId)
    return {
        formId: saveFormResult.insertedId,
        saved: saveFormResult.acknowledged,
        pressCodeList: dtoIn.pressCodeList
    }
}

async function _tryFindProductByCode(code) {
    let result
    try {
        result = await db.products.find({code: code})
    } catch (e) {
        console.log(e)
    }
    return result
}

function _fillForm(form, productId) {
    return {
        productId: ObjectId(productId),
        code: form.code,
        mouldingType: form.mouldingType,
        weight: form.weight,
        cycleLength: form.cycleLength,
        numberAvailable: form.numberAvailable,
    }
}

async function _trySaveForm(form) {
    let result
    try {
        result = await db.forms.insertOne(form)
    } catch (e) {
        console.log(e)
    }
    return result
}

async function _tryUpdatePresses(pressCodeList, formId) {
    try {
        const result = await db.presses.updateMany(
            {code: {$in: pressCodeList}},
            {$push: {compatibleFormIds: formId}}
        )
        console.log("Update presses result:", result)
    } catch (e) {
        console.log(e)
    }
}

function _getPressCodeList(pressList) {
    return pressList.map(press => press.code)
}