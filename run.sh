echo "+-----------------------------+"
echo "| Shutting down containers... |"
echo "+-----------------------------+"
docker-compose --file docker-compose.yml down

echo "+---------------------+"
echo "| Removing volumes... |"
echo "+---------------------+"
docker volume rm bielik_mongo_rs_1_data bielik_mongo_rs_2_data bielik_mongo_rs_3_data bielik_mongo_rs_1_config bielik_mongo_rs_2_config bielik_mongo_rs_3_config

echo "+------------------------------------------+"
echo "| Building Mongo image with npm modules... |"
echo "+------------------------------------------+"
docker build -t mongo-with-npm .

echo "+------------------------+"
echo "| Starting containers... |"
echo "+------------------------+"
docker-compose --file docker-compose.yml up -d

echo "+------------------------------------+"
echo "| Waiting for containers to start... |"
echo "+------------------------------------+"
sleep 5

docker exec -it mongo1 /scripts/00_init_db.sh

echo "+-----------------------+"
echo "| Restarting express... |"
echo "+-----------------------+"
docker restart mongo-express-1
docker restart mongo-express-2
docker restart mongo-express-3
echo "Express restarted."