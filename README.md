<h1>Pozn. k automatizaci vytvoření projektu, skriptům</h2>
<div>
    <p>Projekt by měl být spuštěn pomocí skriptu <a href="run.sh">run.sh</a>. případně
        pomocí <a href="run.bat">run.bat</a>
    </p>
    <p>Automaticky bude spuštěno vytvoření docker image (MongoDB obsahující faker-js npm modul), nasazení
        kontejnerů, vytvoření databáze, testovacích dat, budou spuštěny připravené query a jejich výsledky
        exportovány do souborů. Nakonec bude vyvolán dotaz k potvrzení, či zamítnutí smazání databáze.
    </p>
    <p>Pozor na případné stejně pojmenované kontejnery v Dockeru :)</p>
</div>

<h2>Vše potřebné v <a href="projekt.html">projekt.html</a></h2>