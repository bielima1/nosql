const { faker } = require('@faker-js/faker')
const NUMBER_OF_WORKERS = _getRandomNumber(50000, 100000)
const NUMBER_OF_HALLS = _getRandomNumber(1000, 2000)
const NUMBER_OF_PRODUCTS_FORMS = _getRandomNumber(50000, 100000)
const NUMBER_OF_PRESSES = _getRandomNumber(25000, 50000)
const COLLECTIONS = {
    workers: "workers",
    halls: "halls",
    products: "products",
    forms: "forms",
    presses: "presses"
}
const JOBS = {
    foreman: "Foreman",
    checker: "Checker",
    operator: "Operator"
}
const MOULDING_TYPES = {
    compression: "Compression",
    extrusion: "Extrusion",
    injection: "Injection"
}
const jobArray = [ JOBS.foreman, JOBS.checker, JOBS.operator ]
const mouldingTypeArray = [ MOULDING_TYPES.compression, MOULDING_TYPES.extrusion, MOULDING_TYPES.injection ]

use("rubberMat")

_generateData(COLLECTIONS.workers, NUMBER_OF_WORKERS)
const foremen = db.workers.find({ job: JOBS.foreman }).toArray()
const checkers = db.workers.find({ job: JOBS.checker }).toArray()
const operators = db.workers.find({ job: JOBS.operator }).toArray()
_generateData(COLLECTIONS.halls, NUMBER_OF_HALLS)
const halls = db.halls.find().toArray()
_generateData(COLLECTIONS.products, NUMBER_OF_PRODUCTS_FORMS)
_generateData(COLLECTIONS.forms, NUMBER_OF_PRODUCTS_FORMS)
const compressionForms = db.forms.find({ mouldingType: MOULDING_TYPES.compression }).toArray()
const extrusionForms = db.forms.find({ mouldingType: MOULDING_TYPES.extrusion }).toArray()
const injectionForms = db.forms.find({ mouldingType: MOULDING_TYPES.injection }).toArray()
_generateData(COLLECTIONS.presses, NUMBER_OF_PRESSES)

function _generateData(entity, numOfEntities) {
    print("Generating", numOfEntities, `random ${entity}.`)
    const generatedDocumentsCount = _insertData(entity, numOfEntities)
    print(generatedDocumentsCount, `${entity} generated successfully.`)
}

function _insertData(collectionName, numOfDocuments) {
    const collection = db.getCollection(collectionName)
    let documents
    if (collectionName === COLLECTIONS.forms) {
        documents = _generateForms(numOfDocuments)
    } else {
        const generatingFunc = _getGeneratingFunction(collectionName)
        documents = _generateDocuments(numOfDocuments, generatingFunc)
    }

    _tryInsertMany(collection, documents)

    return collection.countDocuments()
}

function _getGeneratingFunction(collectionName) {
    let func
    switch (collectionName) {
        case COLLECTIONS.workers:
            func = _getRandomWorker
            break
        case COLLECTIONS.halls:
            func = _getRandomHall
            break
        case COLLECTIONS.products:
            func = _getRandomProduct
            break
        case COLLECTIONS.forms:
            func = _getRandomForm
            break
        case COLLECTIONS.presses:
            func = _getRandomPress
            break
    }
    return func
}

function _generateDocuments(numOfDocuments, generatingFunc) {
    const documents = []
    for (let i = 0; i < numOfDocuments; i++) {
        documents.push(generatingFunc())
    }

    return documents
}

function _generateForms(numOfDocuments) {
    const products = db.products.find().toArray()

    const documents = []
    for (let i = 0; i < numOfDocuments; i++) {
        documents.push(_getRandomForm(products[i]))
    }

    return documents
}

function _tryInsertMany(collection, documents) {
    try {
        collection.insertMany(documents)
    } catch (e) {
        print("Some documents may have FAILED the validation!")
        print(e)
    }
}

function _getRandomWorker() {
    return {
        job: _getRandomArrayElement(jobArray),
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        hireDate: faker.date.past(20),
        salary: Double(faker.datatype.number({ min: 10000, max: 100000, precision: 0.01 }))
    }
}

function _getRandomHall() {
    const foreman = _getRandomArrayElement(foremen)

    return {
        "foremanId" : foreman._id,
        "code" : faker.helpers.regexpStyleStringParse('HL-[0-9][0-9]'),
        "area" : Double(faker.datatype.number({ min: 20, max: 100, precision: 0.01 })),
        "floor" : faker.datatype.number({ min: 0, max: 3 })
    }
}

function _getRandomProduct() {
    const checker = _getRandomArrayElement(checkers)
    const materialCode = faker.helpers.replaceSymbols('******')
    const productCode = faker.helpers.replaceSymbols('??######')
    const numStocked = faker.datatype.number({ min: 0, max: 1000000 })
    const numProduced = faker.datatype.number({ min: numStocked, max: 1000000 })

    return {
        "_id": ObjectId(),
        "checkerId" : checker._id,
        "code" : `PR-${materialCode}-${productCode}`,
        "name" : faker.commerce.productName(),
        "material" : materialCode,
        "length" : Double(faker.datatype.number({ min: 1, max: 500, precision: 0.01 })),
        "width" : Double(faker.datatype.number({ min: 1, max: 500, precision: 0.01 })),
        "weight" : Double(faker.datatype.number({ min: 1, max: 5000, precision: 0.01 })),
        "numProduced" : numProduced,
        "numStocked" : numStocked
    }
}

function _getRandomForm(product) {
    return {
        "productId" : product._id,
        "code" : `FM-${faker.helpers.replaceSymbols('??##')}-${faker.helpers.replaceSymbols('###')}-${faker.helpers.replaceSymbols('######')}`,
        "mouldingType" : _getRandomArrayElement(mouldingTypeArray),
        "weight" : Double(faker.datatype.number({ min: 1, max: 5000, precision: 0.01 })),
        "cycleLength" : faker.datatype.number({ min: 60, max: 600, precision: 10 }),
        "numberAvailable": faker.datatype.number({ min: 0, max: 20 })
    }
}

function _getRandomPress() {
    const numOfForms = faker.datatype.number({ min: 0, max: 10 })
    const numOfWorkers = faker.datatype.number({ min: 0, max: 50 })
    const mouldingType = _getRandomArrayElement(mouldingTypeArray);
    const forms = _getFormsByMouldingType(mouldingType)
    const hall = _getRandomArrayElement(halls)
    const formArray = _getRandomArrayElements(forms, numOfForms)
    const workerArray = _getRandomArrayElements(operators.concat(foremen), numOfWorkers)

    return {
        "hallId" : hall._id,
        "compatibleFormIds" : _getIdsFromDocuments(formArray),
        "qualifiedWorkerIds" : _getIdsFromDocuments(workerArray),
        "mouldingType" : mouldingType,
        "code" : `PS-${faker.helpers.replaceSymbols('???')}-${faker.helpers.replaceSymbols('####')}`,
        "acquisitionDate" : faker.date.past(20)
    }
}

function _getFormsByMouldingType(mouldingType) {
    switch (mouldingType) {
        case MOULDING_TYPES.compression:
            return compressionForms
        case MOULDING_TYPES.extrusion:
            return extrusionForms
        case MOULDING_TYPES.injection:
            return injectionForms
    }
}

function _getIdsFromDocuments(documents) {
    return documents.map(document => document._id)
}

function _getRandomArrayElements(array, numOfElements) {
    const result = []
    for (let i = 0; i < numOfElements; i++) {
        result.push(_getRandomArrayElement(array))
    }

    return result
}

function _getRandomArrayElement(array) {
    const index = Math.floor(Math.random() * array.length)

    return array[index]
}

function _getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}