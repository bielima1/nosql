#!/bin/bash

echo "+-----------------------------------------+"
echo "| Initiating replication configuration... |"
echo "+-----------------------------------------+"
mongosh --quiet --file /scripts/10_replication_config.js

echo "+-------------------------------------------------------------------+"
echo "| Waiting 30 seconds for replication configuration to be applied... |"
echo "+-------------------------------------------------------------------+"
sleep 30

echo "+------------------------------------------------+"
echo "| Creating collections and validation schemas... |"
echo "+------------------------------------------------+"
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /scripts/20_validation_schemas.js

echo "+---------------------+"
echo "| Creating indices... |"
echo "+---------------------+"
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /scripts/30_create_indices.js

echo "+----------------------------+"
echo "| Generating testing data... |"
echo "+----------------------------+"
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /scripts/40_generate_data.js

echo "+-----------------------+"
echo "| Testing mocked API... |"
echo "+-----------------------+"
echo "Testing API 1..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /api/api_1_form_acquisition.js
echo "Testing API 2..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /api/api_2_hall_closure.js
echo "Testing completed."

echo "+----------------------+"
echo "| Executing queries... |"
echo "+----------------------+"
echo "Querying first..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /queries/query_1.js
echo "Querying second..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /queries/query_2.js
echo "Querying third..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /queries/query_3.js
echo "Querying fourth..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /queries/query_4.js
echo "Querying fifth..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /queries/query_5.js
echo "Querying fifth, vol.2..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /queries/query_5_v2.js
echo "Querying validation tests..."
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /queries/validation_tests.js
echo "Queries completed."

echo "+----------------------------+"
echo "| Exporting query results... |"
echo "+----------------------------+"
mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query1result --out=/results/query_1_result.json
mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query1explain --out=/results/query_1_execution_plan.json

mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query2result --out=/results/query_2_result.json
mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query2explain --out=/results/query_2_execution_plan.json

mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query3result --out=/results/query_3_result.json
mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query3explain --out=/results/query_3_execution_plan.json

mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query4result --out=/results/query_4_result.json
mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query4explain --out=/results/query_4_execution_plan.json

mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query5result --out=/results/query_5_result.json
mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query5explain --out=/results/query_5_execution_plan.json

mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query5result2 --out=/results/query_5_v2_result.json
mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=query5explain2 --out=/results/query_5_v2_execution_plan.json

mongoexport --uri="mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --collection=validationResult --out=/results/validation_result.json
echo "Query results exported."

echo "+-----------------------------------------+"
echo "| Dropping query results from database... |"
echo "+-----------------------------------------+"
mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/rubberMat?replicaSet=mongoReplicaSet" --quiet <<EOF
db.query1result.drop()
db.query1explain.drop()
db.query2result.drop()
db.query2explain.drop()
db.query3result.drop()
db.query3explain.drop()
db.query4result.drop()
db.query4explain.drop()
db.query5result.drop()
db.query5explain.drop()
db.query5result2.drop()
db.query5explain2.drop()
db.validationResult.drop()
exit
EOF
echo "Auxiliary result collections dropped."

echo "+---------------------------------+"
echo "| Backing up the database data... |"
echo "+---------------------------------+"
mongodump

echo "+----------------------+"
echo "| Dropping database... |"
echo "+----------------------+"
while true; do
    read -p "Do you want to drop the database (default N)? [Y/N]: " yn
    case $yn in
        [Yy]* ) mongosh "mongodb://mongo1:27017,mongo2:27017,mongo3:27017/?replicaSet=mongoReplicaSet" --quiet --file /scripts/50_drop_database.js; echo "Database dropped."; break;;
        [Nn]* ) echo "Database kept."; break;;
        * ) echo "Database kept."; break;;
    esac
done