let config = {
    "_id": "mongoReplicaSet",
    "version": 1,
    "members": [
        {
            "_id": 1,
            "host": "mongo1:27017",
            "priority": 4
        },
        {
            "_id": 2,
            "host": "mongo2:27017",
            "priority": 3
        },
        {
            "_id": 3,
            "host": "mongo3:27017",
            "priority": 2
        }
    ]
};
print("Configuration:")
print(config)
rs.initiate(config, { force: true });
print("Configuration initiated.")