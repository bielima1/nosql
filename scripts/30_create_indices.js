use("rubberMat")

db.workers.createIndex({ job: 1 })

db.forms.createIndex({ productId: 1 })
db.forms.createIndex({ mouldingType: 1 })

db.products.createIndex({ _id: 1, numProduced: 1, numStocked: 1 })

db.presses.createIndex({ hallId: 1 })
db.presses.createIndex({ hallId: 1, mouldingType: 1 })
db.presses.createIndex({ hallId: 1, acquisitionDate: 1 })

print("Indices created.")