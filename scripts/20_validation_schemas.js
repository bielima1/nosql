const validators = {
    workers: {"$jsonSchema":{"bsonType":"object","title":"Worker object validation schema","required":["job","name","surname","hireDate","salary"],"properties":{"hireDate":{"bsonType":"date","description":"Hire date is mandatory."},"surname":{"bsonType":"string","pattern":"[a-zA-Z]+[a-zA-Z ,.'-]*","description":"Surname is mandatory."},"name":{"bsonType":"string","pattern":"[a-zA-Z]+[a-zA-Z ,.'-]*","description":"Name is mandatory."},"job":{"bsonType":"string","enum":["Foreman","Checker","Operator"],"description":"Job position is mandatory. Must be either foreman, checker or operator."},"salary":{"bsonType":"double","minimum":0,"description":"Salary is mandatory. Can only be a positive value."}}}},
    halls: {"$jsonSchema":{"bsonType":"object","title":"Hall object validation schema","required":["foremanId","code","area","floor"],"properties":{"area":{"bsonType":"double","minimum":0,"description":"Are size in square metres. Is mandatory."},"code":{"bsonType":"string","pattern":"HL-[0-9]{2}","description":"Code is mandatory."},"floor":{"bsonType":"int","minimum":0,"description":"Factory floor number. Is mandatory."},"foremanId":{"bsonType":"objectId","description":"ID of the responsible foreman."}}}},
    products: {"$jsonSchema":{"bsonType":"object","title":"Product object validation schema","required":["code","name","material"],"properties":{"weight":{"bsonType":"double","minimum":0,"description":"Weight in grams. Positive values only."},"code":{"bsonType":"string","pattern":"PR-[A-Z0-9]{6}-[A-Z]{2}[0-9]{6}","description":"Code is mandatory."},"material":{"bsonType":"string","pattern":"[A-Z0-9]{6}","description":"Material is mandatory."},"numProduced":{"bsonType":"int","minimum":0,"description":"Total number produced. Positive values only."},"name":{"bsonType":"string","description":"Name is mandatory."},"length":{"bsonType":"double","minimum":0,"description":"Length in millimetres. Positive values only."},"width":{"bsonType":"double","minimum":0,"description":"Width in millimetres. Positive values only."},"numStocked":{"bsonType":"int","minimum":0,"description":"Number in store. Positive values only."},"checkerId":{"bsonType":"objectId","description":"ID of the worker responsible for dimension checks."}}}},
    forms: {"$jsonSchema":{"bsonType":"object","title":"Form object validation schema","required":["productId","code","mouldingType"],"properties":{"code":{"bsonType":"string","pattern":"FM-[A-Z]{2}[0-9]{2}-[0-9]{3}-[0-9]{6}","description":"Code is mandatory."},"productId":{"bsonType":"objectId","description":"ID of the product manufactured in the form. Is mandatory."},"mouldingType":{"bsonType":"string","enum":["Compression","Extrusion","Injection"],"description":"Moulding type is mandatory. Must be either compression, extrusion or injection."},"weight":{"bsonType":"double","minimum":0,"description":"Weight of the form in kilograms. Positive values only."},"cycleLength":{"bsonType":"int","minimum":0,"description":"Batch production cycle length in seconds. Positive values only."},"numberAvailable":{"bsonType":"int","minimum":0,"description":"Number of available forms. Positive values only."}}}},
    presses: {"$jsonSchema":{"bsonType":"object","title":"Press object validation schema","required":["mouldingType","code","acquisitionDate"],"properties":{"compatibleFormIds":{"bsonType":"array","description":"List of IDs of compatible forms.","items":{"bsonType":"objectId"}},"acquisitionDate":{"bsonType":"date","description":"Acquisition date is mandatory."},"code":{"bsonType":"string","pattern":"PS-[A-Z]{3}-[0-9]{4}","description":"Code is mandatory."},"hallId":{"bsonType":["objectId","null"],"description":"ID of the hall where the press is located."},"qualifiedWorkerIds":{"bsonType":"array","description":"List of IDs of workers qualified to work with the press.","items":{"bsonType":"objectId"}},"mouldingType":{"bsonType":"string","enum":["Compression","Extrusion","Injection"],"description":"Moulding type is mandatory. Must be either compression, extrusion or injection."}}}}
}

use("rubberMat")
print("RubberMat database created.")
print("")

print("Worker validation schema:")
print(validators.workers)
db.createCollection("workers", {"validator": validators.workers})
print("Worker schema created.")
print("")

print("Hall validation schema:")
print(validators.halls)
db.createCollection("halls", {"validator": validators.halls})
print("Hall schema created.")
print("")

print("Product validation schema:")
print(validators.products)
db.createCollection("products", {"validator": validators.products})
print("Product schema created.")
print("")

print("Form validation schema:")
print(validators.forms)
db.createCollection("forms", {"validator": validators.forms})
print("Form schema created.")
print("")

print("Press validation schema:")
print(validators.presses)
db.createCollection("presses", {"validator": validators.presses})
print("Press schema created.")