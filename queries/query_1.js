use("rubberMat")

const hall = db.halls.aggregate([{ $sample: { size: 1 } }]).toArray()
const result = db.presses.find({ hallId: hall[0]._id, acquisitionDate: { $lt: ISODate('2015-01-01T00:00:00.000Z') } }).sort({ acquisitionDate: 1 })

db.query1result.insertMany(result.toArray())
db.query1explain.insertOne(result.explain())