use("rubberMat")

const result = db.forms.aggregate([
    {
        $match: {mouldingType: "Extrusion"}
    },
    {
        $lookup: {
            from: "products",
            localField: "productId",
            foreignField: "_id",
            as: "product"
        }
    },
    {
        $project: {
            "_id": 0,
            "code": 1,
            "cycleLength": 1,
            "product.weight": 1,
            "product.numProduced": 1,
        }
    },
    {
        $unwind: "$product"
    },
    {
        $group: {
            _id: "$code",
            totalProductionTime: {$sum: {$round: [{$divide: [{$multiply: ["$cycleLength", "$product.numProduced"]}, 3600]}, 2]}},
            totalWeightOfProducts: {$sum: {$round: [{$divide: [{$multiply: ["$product.weight", "$product.numProduced"]}, 1000000]}, 2]}}
        }
    },
    {
        $sort: {_id: 1}
    }
])

db.query5result.insertMany(result.toArray())
db.query5explain.insertOne(result.explain())