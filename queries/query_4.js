use("rubberMat")

const result = db.forms.aggregate([
    {
        $match: {mouldingType: "Compression"}
    },
    {
        $project: {_id: 0, productId: 1}
    },
    {
        $lookup: {
            from: "products",
            let: {id: "$productId"},
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                {$eq: ["$_id", "$$id"]},
                                {$gte: ["$numProduced", 500000]},
                                {$gte: ["$numStocked", 100000]}
                            ]
                        }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        checkerId: 1,
                        code: 1,
                        name: 1,
                        numProduced: 1,
                        numStocked: 1
                    }
                }
            ],
            as: "product"
        }
    },
    {
        $unwind: "$product"
    },
    {
        $addFields: {
            checkerId: "$product.checkerId",
            code: "$product.code",
            name: "$product.name",
            numProduced: "$product.numProduced",
            numStocked: "$product.numStocked"
        }
    },
    {
        $project: {
            productId: 1,
            checkerId: 1,
            code: 1,
            name: 1,
            numProduced: 1,
            numStocked: 1
        }
    },
    {
        $lookup: {
            from: "workers",
            localField: "checkerId",
            foreignField: "_id",
            as: "checker"
        }
    },
    {
        $unwind: "$checker"
    },
    {
        $project: {checkerId: 0}
    }
])

db.query4result.insertMany(result.toArray())
db.query4explain.insertOne(result.explain())