use("rubberMat")

const result = db.forms.aggregate([
    {
        $group: {
            _id: "$mouldingType",
            numberOfFormTypes: { $count: {} },
            totalNumberAvailable: { $sum: "$numberAvailable" }
        }
    }
])

db.query2result.insertMany(result.toArray())
db.query2explain.insertOne(result.explain())