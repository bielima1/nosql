use("rubberMat")
let emptyWorker
try {
    db.workers.insertOne({})
} catch (e) {
    emptyWorker = e.errInfo
}

let invalidWorker
try {
    db.workers.insertOne({
        "job": "Boss",
        "name": ".John",
        "surname": ".Doe",
        "hireDate": 1,
        "salary": -10
    })
} catch (e) {
    invalidWorker = _expandObject(e.errInfo)
}

let emptyHall
try {
    db.halls.insertOne({})
} catch (e) {
    emptyHall = e.errInfo
}

let invalidHall
try {
    db.workers.insertOne({
        "foremanId": 10,
        "code": "Aaaaaaa",
        "area": -10,
        "floor": -10
    })
} catch (e) {
    invalidHall = _expandObject(e.errInfo)
}

let emptyProduct
try {
    db.products.insertOne({})
} catch (e) {
    emptyProduct = e.errInfo
}

let invalidProduct
try {
    db.workers.insertOne({
        "checkerId": "ASffgs",
        "code": "ailgl",
        "name": "......",
        "material": 1,
        "length": -10,
        "width": "jksdf",
        "weigth": undefined,
        "numProduced": -50,
        "numStocked": "hundred"
    })
} catch (e) {
    invalidProduct = _expandObject(e.errInfo)
}

let emptyForm
try {
    db.forms.insertOne({})
} catch (e) {
    emptyForm = e.errInfo
}

let invalidForm
try {
    db.forms.insertOne({
        "productId": 1,
        "code": null,
        "mouldingType": "Extruze",
        "weight": "heavy",
        "cycleLength": 10.256,
        "numberAvailable": null
    })
} catch (e) {
    invalidForm = _expandObject(e.errInfo)
}

let emptyPress
try {
    db.presses.insertOne({})
} catch (e) {
    emptyPress = e.errInfo
}

let invalidPress
try {
    db.presses.insertOne({
        "hallId": [],
        "compatibleFormIds": ["aaa", "bbb"],
        "qualifiedWorkerIds": 1,
        "mouldingType": 2,
        "code": null,
        "acquisitionDate": 2016
    })
} catch (e) {
    invalidPress = _expandObject(e.errInfo)
}

db.validationResult.insertMany([
    emptyWorker, invalidWorker,
    emptyHall, invalidHall,
    emptyProduct, invalidProduct,
    emptyForm, invalidForm,
    emptyPress, invalidPress
])

function _expandObject(object) {
    return JSON.parse(JSON.stringify(object, null, 4))
}