use("rubberMat")

const result = db.halls.aggregate([
    {
        $sample: {size: 1}
    },
    {
        $project: {
            _id: 1,
            code: 1
        }
    },
    {
        $lookup: {
            from: "presses",
            let: {id: "$_id"},
            pipeline: [
                {
                    $match:
                        {
                            $expr:
                                {
                                    $and:
                                        [
                                            {$eq: ["$hallId", "$$id"]},
                                            {mouldingType: "Injection"}
                                        ]
                                }
                        }
                },
                {
                    $project: {_id: 0, qualifiedWorkerIds: 1}
                }
            ],
            as: "press"
        }
    },
    {
        $unwind: "$press"
    },
    {
        $group: {
            _id: "$code",
            allIds: {$push: "$press.qualifiedWorkerIds"}
        }
    },
    {
        $project: {
            qualifiedWorkerIds: {
                $reduce:
                    {
                        input: "$allIds",
                        initialValue: [],
                        in: {$concatArrays: ['$$value', '$$this']}
                    }
            }
        }
    },
    {
        $lookup: {
            from: "workers",
            localField: "qualifiedWorkerIds",
            foreignField: "_id",
            as: "qualifiedWorkers"
        }
    },
    {
        $addFields: {
            hall: "$_id",
            pressType: "Injection"
        }
    },
    {
        $project: {
            _id: 0,
            hall: 1,
            pressType: 1,
            qualifiedWorkers: 1
        }
    }
])

db.query3result.insertOne(result.toArray()[0])
db.query3explain.insertOne(result.explain())